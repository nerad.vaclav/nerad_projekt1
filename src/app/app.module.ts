import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NgZorroAntdModule, NZ_I18N, cs_CZ, en_US, en_GB } from 'ng-zorro-antd';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { registerLocaleData } from '@angular/common';
registerLocaleData(cs);
import cs from '@angular/common/locales/cs';

//ngx translate
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

//Pages

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PageOneComponent } from './pages/page-one/page-one.component';
import { PageTwoComponent } from './pages/page-two/page-two.component';
import { PageThreeComponent } from './pages/page-three/page-three.component';
import { PageFourComponent } from './pages/page-four/page-four.component';
import { PageFiveComponent } from './pages/page-five/page-five.component';
import { PageSixComponent } from './pages/page-six/page-six.component';
import { PageSevenComponent } from './pages/page-seven/page-seven.component';

//Components
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { CommitsComponent } from './pages/commits/commits.component';
import { PageAboutComponent } from './pages/about/page-about.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';




@NgModule({
  declarations: [
    AppComponent,
    PageOneComponent,
    PageTwoComponent,
    PageThreeComponent,
    PageFourComponent,
    PageFiveComponent,
    PageSixComponent,
    PageSevenComponent,
    NavBarComponent,
    CommitsComponent,
    PageAboutComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot([
      { path: 'page-one', component: PageOneComponent, pathMatch: 'full' },
      { path: 'page-two', component: PageTwoComponent, pathMatch: 'full' },
      { path: 'page-three', component: PageThreeComponent, pathMatch: 'full' },
      { path: 'page-four', component: PageFourComponent, pathMatch: 'full' },
      { path: 'page-five', component: PageFiveComponent, pathMatch: 'full' },
      { path: 'page-six', component: PageSixComponent, pathMatch: 'full' },
      { path: 'page-seven', component: PageSevenComponent, pathMatch: 'full' },
      { path: 'commits', component: CommitsComponent, pathMatch: 'full' },
      { path: 'page-about', component: PageAboutComponent, pathMatch: 'full' },


    ]),
    NgZorroAntdModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
  })
  ],
  providers: [{ provide: NZ_I18N, useValue: en_GB}],
  bootstrap: [AppComponent]
})
export class AppModule { }

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

