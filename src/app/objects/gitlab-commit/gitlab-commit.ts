export class GitlabCommit {
    id: string;
    short_id: string;
    title: string;
    author_name: string;
    author_email: string;
    committer_name: string;
    committer_email: string;
    created_at: string;
    message: string;
    parent_ids: Array<string>;
  }
  