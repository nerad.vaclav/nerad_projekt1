import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GitlabCommit } from '../../objects/gitlab-commit/gitlab-commit';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Filter } from 'src/app/objects/filter/filter';



@Component({
    selector: 'app-commits',
    templateUrl: './commits.component.html',
    styleUrls: ['./commits.component.css']
})
export class CommitsComponent implements OnInit {

    mess = "";
    data: GitlabCommit[];
    isWorking: boolean = false;
    validateForm: FormGroup;
    datestring;
    filter: Filter = new Filter();
    listOfOption = [];

    //#region Lifecycle
    constructor(
        private _httpClient: HttpClient,
        private fb: FormBuilder,
    ) { }

    async ngOnInit() {
        this.createForm();
        await this.getAllCommits();        
    }

    //#endregion

    //#region Events 

    onDateChange(result: Date): void {
        if (result) {
            this.datestring = result.getFullYear() + "-" + ("0" + (result.getMonth() + 1)).slice(-2) + "-" + ("0" + result.getDate()).slice(-2);
        }
        else {
            this.datestring = "";
        }
    }

    onResetFilter() {
        this.filter = new Filter();
        this.mess = "";
        this.getAllCommits();
    }

    //#endregion

    //#region Data work

    async getAllCommits(filter: boolean = false) {
        this.isWorking = true;

        var commits = await this.getCommits();

        while (commits.length > 0 && commits.length % 10 === 0) {
            const page = Math.floor(commits.length / 10) + 1
            const res = await this.getCommits(page);
            commits.push(...res)
        }

        this.getEmail(commits);

        if (filter) {
            for (const i in this.validateForm.controls) {
                this.validateForm.controls[i].markAsDirty();
                this.validateForm.controls[i].updateValueAndValidity();
            }

            if (!this.validateForm.valid) {
                return;
            }

            commits = this.filterCommits(this.datestring, commits);
        }

        this.data = commits;
        this.isWorking = false;
    }

    filterCommits(date: string, commits: GitlabCommit[]) {
        commits = commits.filter(commit => commit.committer_email.includes(this.filter.email));
        commits = commits.filter(commit => commit.created_at.includes(date));
        this.mess = "";
        for (var data of commits) {
            this.mess += data.message.replace(/\r?\n|\r/g, "") + "; ";
        }

        return commits;
    }

    getEmail(commit: GitlabCommit[]) {
        for (var email of commit) {
            this.listOfOption.push(email.committer_email);
        }
        this.listOfOption = this.listOfOption.filter((v, i, a) => a.indexOf(v) === i);
    };

    async getCommits(page: number = 1) {
        const result = await this._httpClient.get<Array<GitlabCommit>>('https://gitlab.com/api/v4/projects/17831171/repository/commits?private_token=Qqbiy_kBt9HubWBres3-&per_page=10&page=' + page)
            .toPromise<Array<GitlabCommit>>();

        return result;
    }

    //#endregion

    //#region Forms

    createForm() {
        this.validateForm = this.fb.group({
            email: [null, [Validators.required]],
            date: [null, [Validators.required]],
        });
    }

    //#endregion
}

